// Author: James Deasley
// Student ID: 19334721

import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.io.File;
import java.io.FileInputStream;

import tcdIO.*;

/**
 *
 * Client class
 *
 * An instance accepts user input
 *
 */
public class App extends Node {
	static final int DEFAULT_SRC_PORT = 51511;
	static final int DEFAULT_DST_PORT = 51510;
	static final String DEFAULT_DST_NODE = "localhost";

    final int SENDER = 1;
    final int RECEIVER = 2;
	
	Terminal terminal;
	InetSocketAddress dstAddress;

    int mode;
    String myName;

	/**
	 * Constructor
	 *
	 * Attempts to create socket at given port and create an InetSocketAddress for the destinations
	 */
	App(Terminal terminal, String dstHost, int dstPort, int srcPort, String myName, int mode) {
		try {
			this.terminal = terminal;
			dstAddress= new InetSocketAddress(dstHost, dstPort);
			socket= new DatagramSocket(srcPort);
            this.myName = myName;
            this.mode = mode;
			listener.go();
		}
		catch(java.lang.Exception e) {e.printStackTrace();}
	}


	/**
	 * Assume that incoming packets contain a String and print the string.
	 */
	public synchronized void onReceipt(DatagramPacket packet) {

        try {

			PacketContent content = PacketContent.fromDatagramPacket(packet);
            String srcName = ((Message)content).getSrcName();
            String dstName = ((Message)content).getDstName();

			terminal.println("\nReceived packet from: " + srcName);

            // If the packet came from the localhost, i.e. the App
            if(content.getType()==PacketContent.MESSAGE){
                terminal.println(((Message)content).toString());
            }

		}
		catch(Exception e) {e.printStackTrace();}
		
	}


	/**
	 * Sender Method
	 *
	 */
	public synchronized void start() throws Exception {

		terminal.println("App name: " + myName);

        Message msg;
        DatagramPacket packet;
        
        // Enter sender mode
        // Destination is hardcoded to "trinity2" for now.
        // Type in message, then send it to the Service for forwarding.
        if(mode == SENDER){

            String dstName = "trinity2";
            String msgText;

            while(true){
                terminal.println("\nSending to: " + dstName);
                msgText = terminal.readString("Message: ");

                terminal.println("Sending packet to Service at: " + dstAddress.toString());

                msg = new Message(dstName, myName, msgText);

                packet = msg.toDatagramPacket();
                packet.setSocketAddress(dstAddress);
                socket.send(packet);
            }
        }
        // Enter receiver mode.
        // For now, hardcoded to receive traffic for "trinity2"
        else{

            // Send message to router that I intend to receive traffic for the String "trinity2"
            msg = new Message("service", "trinity2", "localhost:51511");

            packet = msg.toDatagramPacket();
            packet.setSocketAddress(dstAddress);
            socket.send(packet);

        }

        this.wait();
	}


	/**
	 * Test method
	 *
	 * Sends a packet to a given address
     *
     * args[0]: name, e.g. "trinity"
     * args[1]: mode
     *
	 */
	public static void main(String[] args) {
		try {
			Terminal terminal = new Terminal("Application");
			(new App(terminal, DEFAULT_DST_NODE, DEFAULT_DST_PORT, DEFAULT_SRC_PORT, args[0], Integer.parseInt(args[1]))).start();
			System.out.println("Program completed");
		} catch(java.lang.Exception e) {e.printStackTrace();}
	}
}
