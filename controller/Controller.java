// Author: James Deasley
// Student ID: 19334721

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.net.InetSocketAddress;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;

import tcdIO.Terminal;

public class Controller extends Node {
	static final int DEFAULT_PORT = 51510;

    // Map of maps of addresses of next hop.
    // Indexed using get(-destinationName-).get(-senderName-).
    // This will return the address of the next hop in the path from the sender to the destination.
    private Map<String, Map<InetSocketAddress, String>> forwardingTable;

	/*
	 *
	 */
	Controller(int port) {
		try {
			socket= new DatagramSocket(port);
            forwardingTable = new HashMap<String, Map<InetSocketAddress, String>>();
            initialiseTable();
			listener.go();
		}
		catch(java.lang.Exception e) {e.printStackTrace();}
	}

    private void initialiseTable() {
        
        // In and Out values to get to endpoint2
        // This will then be put into a bigger forwarding table to be referenced by name, e.g. "trinity2"
        Map<InetSocketAddress, String> tableToE2 = new HashMap<InetSocketAddress, String>();

        // Where do I forward to get to trinity2 from user1?
        // Want it to get this information in the format:
        //  get("trinity2").get("user1")
        tableToE2.put(new InetSocketAddress("user1", 51510), "router1");
        tableToE2.put(new InetSocketAddress("router1", 51510), "router2");
        tableToE2.put(new InetSocketAddress("router2", 51510), "router3");
        tableToE2.put(new InetSocketAddress("router3", 51510), "user2");

        forwardingTable.put("trinity2", tableToE2);
    }

	/**
	 * Assume that incoming packets contain a String and print the string.
	 */
	public void onReceipt(DatagramPacket packet) {
		try {

            // E1 wants to send message to "trinity2"
            // It doesn't have anything in its table
            // So it sends a message to the Controller to find where to forward to.

            // If it sends the message it intended to send to "trinity2" to the Controller, then we have:
            //  - the InetSocketAddress it came from
            //  - the dst name
            
            PacketContent content = PacketContent.fromDatagramPacket(packet);
            InetSocketAddress srcAddr = (InetSocketAddress)(packet.getSocketAddress());
            String srcName = ((Message)content).getSrcName();
            String dstName = ((Message)content).getDstName();


			System.out.println("\nReceived request from " + srcAddr.getHostName() + " for next hop to '" + dstName + "'");
            DatagramPacket reply;

            String nextHop = forwardingTable.get(dstName).get(srcAddr);
            System.out.println("Next hop: " + nextHop);

            TableUpdate update = new TableUpdate(dstName, srcName, nextHop, ((Message)content).getMsgContent());

            System.out.println("Sending update to " + srcAddr.getHostName());
            reply = update.toDatagramPacket();
            reply.setSocketAddress(packet.getSocketAddress());
            socket.send(reply);
		}
		catch(Exception e) {e.printStackTrace();}
	}


	public synchronized void start() throws Exception {
		System.out.println("Listening on port: " + DEFAULT_PORT);
        System.out.println("Waiting for contact...");
		this.wait();
	}

	public static void main(String[] args) {
		try {
			// Terminal terminal = new Terminal("Router");
			(new Controller(DEFAULT_PORT)).start();
			System.out.println("Program completed");
		} catch(java.lang.Exception e) {e.printStackTrace();}
	}
}
