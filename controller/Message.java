// Author: James Deasley
// Student ID: 19334721

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Class for packet content that represents file information
 *
 */
public class Message extends PacketContent {

	String msgContent;

	/**
	 * Constructor that takes in information about a file.
	 * @param topicName Initial topicName.
	 * @param content Information for publication.
	 */
	Message(String dstName, String srcName, String msgContent) {
        packetType = MESSAGE;
		packetLength = dstName.length() + srcName.length() + msgContent.length();
        this.dstName = dstName;
        this.srcName = srcName;
		this.msgContent = msgContent;
	}

	/**
	 * Constructs an object out of an ObjectInputStream.
	 * @param oin ObjectInputStream containing the parameters needed to create a Message.
	 */
	protected Message(ObjectInputStream oin) {
		try {
            packetType = MESSAGE;
			msgContent = oin.readUTF();
			// packetLength = dstName.length() + srcName.length() + msgContent.length();
		}
		catch(Exception e) {e.printStackTrace();}
	}

	/**
	 * Writes the content into an ObjectOutputStream
	 *
	 */
	protected void toObjectOutputStream(ObjectOutputStream oout) {
		try {
			oout.writeUTF(msgContent);
		}
		catch(Exception e) {e.printStackTrace();}
	}


	/**
	 * Returns the content of the packet as String.
	 *
	 * @return Returns the content of the packet as String.
	 */
	public String toString() {
		return "Packet type: " + packetType + " (Message) - Length: " + packetLength + " - Value:\n" + srcName + " > " + dstName + ": " + msgContent;
	}

    public String getMsgContent(){
        return msgContent;
    }
}
