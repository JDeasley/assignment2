# Flow Forwarding Protocol

### **Student Name:** James Deasley
### **Student ID:** 19334721  

This README contains information pertaining to the practical setup and operation of my Flow Forwarding protocol. It is divided into 2 sections:  

1. **Setup**, with a run through on the Docker setup. This section covers creation of folders and general setup followed by the Docker Compose commands to start everything running.
2. **Using the interface**, with a run through of how to use each of the nodes once they have been fully set up and started.

## 1) Setup
In my report for Assignment 1, I said in my reflection that I would have liked to have used Docker Compose as opposed to running commands line by line to set up and start my containers. I'm excited to say that I have achieved this goal for this assignment, so the Docker setup is rather simple.

I have copied replicas of each of the files into folders called "user1", "user2", "router", and "controller", each folder also containing a Dockerfile to build the image for the named component.

With these folders set up in the same directory as the "docker-compose.yml" file, simply type the following into the command line:

`docker compose build`  
`docker compose up`

Docker Compose handles the rest.

You will then be presented with a stream of information from each component, as well as tcdIO Terminal windows for each user as an interface through which to send messages.

**Note:** You will need to have some way of running the tcdIO Terminal windows set up, whether that is natively or through something like "XMing" on Windows. The Dockerfiles for the user set the DISPLAY environment variable to "docker.host.internal:0.0", which is correct on Windows, but this line in the Dockerfile may need to be edited to be run on a machine with a different OS.

---  

## 2) Using the interface
As with the setup above, thanks to Docker Compose the interface is quite simple.

When you start up the containers, you will see 2 tcdIO Terminal windows, one for the App of User1 and one for the App of User2. User1 only sends messages while User2 only receives messages. You will also see the output from the forwarding Service of each container in the command line window where you entered the `docker compose up` command.

To use the interface, simply type a message where prompted into the User1 App, and then you can track the packet in the command line window as it is sent through all of the forwarding Services on different networks and finally arrives at User2, which outputs the message.

---

**Note:** For viewing PCAP files in Wireshark, I recommend applying the following filters:

For Router1:  
`udp and (ip.src==172.10.0.0/24 or ip.src==172.11.0.0/24 or ip.src==172.14.0.0/24)`  

For Controller:  
`udp and ip.src==172.14.0.0/24`

It is also worth mentioning that Docker Compose also sets up *TCPDump* services which capture traffic on Router1 and the Controller, and then automatically places the PCAP files into the "tcpdump" folder.