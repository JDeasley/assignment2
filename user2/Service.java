// Author: James Deasley
// Student ID: 19334721

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.InetSocketAddress;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;

import tcdIO.Terminal;

public class Service extends Node {
	static final int DEFAULT_PORT = 51510;
    static final int APP_PORT = 51511;

    static final String CONTROLLER_HOST = "controller";

    // Map of topics and addresses of Dashboards which are subsribed to them
    private Map<String, InetSocketAddress> forwardingTable;
    private Map<String, Integer> trafficPorts;

    InetSocketAddress controller;

	/*
	 *
	 */
	Service(int port, String controller_host, int controller_port) {
		try {
			socket= new DatagramSocket(port);
            forwardingTable = new HashMap<>();
            trafficPorts = new HashMap<>();
            controller = new InetSocketAddress(controller_host, controller_port);
			listener.go();
		}
		catch(java.lang.Exception e) {e.printStackTrace();}
	}

	/**
	 * Assume that incoming packets contain a String and print the string.
	 */
	public void onReceipt(DatagramPacket packet) {
		try {

			PacketContent content = PacketContent.fromDatagramPacket(packet);
            String receivedFrom = ((InetSocketAddress)(packet.getSocketAddress())).getHostName();
            String dstName = content.getDstName();
            String srcName = content.getSrcName();

            DatagramPacket forward;

            // We receive MESSAGE packets from everywhere but the Controller.
            if(content.getType()==PacketContent.MESSAGE){

                if(receivedFrom.equals("localhost")){
                    System.out.println("\nReceived packet from App.");
                }
                else{
                    System.out.println("\nReceived packet from external source: " + receivedFrom);
                }

                // If destination is service, the App is signing up to receive traffic for a string, e.g. "trinity2"
                // In this case, we don't need to check if the dstAddress is in the table, because we don't need to send anything back or elsewhere.
                if(receivedFrom.equals("localhost") && dstName.equals("service")){
                    System.out.println("App will now receive traffic for: " + srcName);
                    trafficPorts.put(srcName, ((InetSocketAddress)(packet.getSocketAddress())).getPort());
                    // forwardingTable.put(srcName, new InetSocketAddress("localhost", 51511));
                }
                /* If the destination is NOT a local service, then we will need to forward the packet elsewhere.
                 * In order to send it elsewhere, we first need to check if we have the address of the destination in the forwarding table.
                 * If we don't know the next hop to that address, contact the Controller.
                 * The Controller will then send back a packet at which point we will add the destination address to the table and forward the message to that address.*/
                else if(!forwardingTable.containsKey(dstName) && !trafficPorts.containsKey(dstName)){
                    System.out.println("\nRequesting next hop to '" + dstName + "' from the controller... ");
                    
                    forward = content.toDatagramPacket();
                    forward.setSocketAddress(controller);
                    socket.send(forward);

                    System.out.println("Awaiting response from the controller... ");
                }
                // Otherwise, we forward the packet to its destination.
                else{

                    forward = content.toDatagramPacket();

                    // Destination String is signed up to by the App
                    if(trafficPorts.containsKey(dstName)){
                        System.out.println("Forwarding packet to App: " + dstName);
                        InetSocketAddress appAddr = new InetSocketAddress("localhost", trafficPorts.get(dstName));
                        forward.setSocketAddress(appAddr);
                    }
                    else{
                        System.out.println("Forwarding packet to '" + dstName + "'\nvia: " + forwardingTable.get(dstName));
                        forward.setSocketAddress(forwardingTable.get(dstName));
                    }

                    socket.send(forward);
                }
            }
            // We receive TABLE_UPDATE packets only from the Controller.
            else if(content.getType()==PacketContent.TABLE_UPDATE){
                System.out.println("\nAcquired next hop to '" + dstName + "'.\nAdding to forwarding table.");

                forwardingTable.put(dstName, new InetSocketAddress(((TableUpdate)content).getNextHop(), 51510));

                System.out.println("Forwarding packet to '" + dstName + "'\nvia: " + forwardingTable.get(dstName));

                // Retrieve message as it was saved by the Controller when we sent our address request.
                // Then forward this message to the updated address.
                Message msg = new Message(dstName, srcName, ((TableUpdate)content).getSavedMessage());
                forward = msg.toDatagramPacket();
                forward.setSocketAddress(forwardingTable.get(dstName));
                socket.send(forward);
            }

            

		}
		catch(Exception e) {e.printStackTrace();}
	}


	public synchronized void start() throws Exception {
		System.out.println("Listening on port: " + DEFAULT_PORT);
        System.out.println("Waiting for contact...");
		this.wait();
	}

	public static void main(String[] args) {
		try {
			(new Service(DEFAULT_PORT, CONTROLLER_HOST, DEFAULT_PORT)).start();
			System.out.println("Program completed");
		} catch(java.lang.Exception e) {e.printStackTrace();}
	}
}
