// Author: James Deasley
// Student ID: 19334721

import java.net.DatagramPacket;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * The class is the basis for packet contents of various types.
 *
 *
 */
public abstract class PacketContent {

	// Packet type constants
	public static final byte MESSAGE = 1;
	public static final byte TABLE_UPDATE = 2;

	byte packetType = 0;
	static int packetLength = 0;

    static String dstName;
    static String srcName;

	/**
	 * Constructs an object out of a datagram packet.
	 * @param packet Packet to analyse.
	 */
	public static PacketContent fromDatagramPacket(DatagramPacket packet) {
		PacketContent content= null;

		try {
			byte packetType;
			// byte packetLength;

			byte[] data;
			ByteArrayInputStream bin;
			ObjectInputStream oin;

			data= packet.getData();  // use packet content as seed for stream
			bin= new ByteArrayInputStream(data);
			oin= new ObjectInputStream(bin);

			packetType = oin.readByte();  // read packetType from header of packet
			packetLength = oin.readInt();  // read packetLength from header of packet
			dstName = oin.readUTF();  // read dstName from header of packet
			srcName = oin.readUTF();  // read srcName from header of packet

			switch(packetType) {   // depending on type create content object
			// case ACKPACKET:
			// 	content= new AckPacketContent(oin);
			// 	break;
			// case SUBSCRIBE:
			// 	content= new SubscribeContent(oin);
			// 	break;
			// case PUBLISH:
			// 	content= new PublicationContent(oin);
			// 	break;
			case MESSAGE:
				content = new Message(oin);
				break;
			case TABLE_UPDATE:
				content = new TableUpdate(oin);
				break;
			default:
				content= null;
				break;
			}
			oin.close();
			bin.close();

		}
		catch(Exception e) {e.printStackTrace();}

		return content;
	}


	/**
	 * This method is used to transform content into an output stream.
	 *
	 * @param out Stream to write the content for the packet to.
	 */
	protected abstract void toObjectOutputStream(ObjectOutputStream out);

	/**
	 * Returns the content of the object as DatagramPacket.
	 *
	 * @return Returns the content of the object as DatagramPacket.
	 */
	public DatagramPacket toDatagramPacket() {
		DatagramPacket packet= null;

		try {
			ByteArrayOutputStream bout;
			ObjectOutputStream oout;
			byte[] data;

			bout= new ByteArrayOutputStream();
			oout= new ObjectOutputStream(bout);

			oout.writeByte(packetType);		// write packetType to stream
			oout.writeInt(packetLength);	// write packetLength to stream
			oout.writeUTF(dstName);			// write dstName to stream
			oout.writeUTF(srcName);         // write srcName to stream
			toObjectOutputStream(oout);		// write content to stream depending on packetType

			oout.flush();
			data= bout.toByteArray(); // convert content to byte array

			packet= new DatagramPacket(data, data.length); // create packet from byte array
			oout.close();
			bout.close();
		}
		catch(Exception e) {e.printStackTrace();}

		return packet;
	}


	/**
	 * Returns the content of the packet as String.
	 *
	 * @return Returns the content of the packet as String.
	 */
	public abstract String toString();

	/**
	 * Returns the type of the packet.
	 *
	 * @return Returns the type of the packet.
	 */
	public byte getType() {
		return packetType;
	}

	public String getDstName(){
        return dstName;
    }

    public String getSrcName(){
        return srcName;
    }

}
