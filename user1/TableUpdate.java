// Author: James Deasley
// Student ID: 19334721

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.DatagramPacket;

/**
 * Class for packet content that represents file information
 *
 */
public class TableUpdate extends PacketContent {

	String nextHop;
    String savedMessage;

	/**
	 * Constructor that takes in information about a file.
	 * @param dstName Name of final destination node of the update.
	 * @param srcName Name of source node requesting the update.
	 * @param nextHop Address of the next hop on the route to the destination.
	 * @param savedMessage Message which the Service attempted to send before reqqesting the update.
	 */
	TableUpdate(String dstName, String srcName, String nextHop, String savedMessage) {
        packetType = TABLE_UPDATE;
		packetLength = dstName.length() + srcName.length() + nextHop.length() + savedMessage.length();
        this.dstName = dstName;
        this.srcName = srcName;
		this.nextHop = nextHop;
        this.savedMessage = savedMessage;
	}

	/**
	 * Constructs an object out of an ObjectInputStream.
	 * @param oin ObjectInputStream containing the parameters needed to create a Message.
	 */
	protected TableUpdate(ObjectInputStream oin) {
		try {
            packetType = TABLE_UPDATE;
			nextHop = oin.readUTF();
            savedMessage = oin.readUTF();
			// packetLength = dstName.length() + srcName.length() + nextHop.length() + savedMessage.length();
		}
		catch(Exception e) {e.printStackTrace();}
	}

	/**
	 * Writes the content into an ObjectOutputStream
	 *
	 */
	protected void toObjectOutputStream(ObjectOutputStream oout) {
		try {
			oout.writeUTF(nextHop);
			oout.writeUTF(savedMessage);
		}
		catch(Exception e) {e.printStackTrace();}
	}


	/**
	 * Returns the content of the packet as String.
	 *
	 * @return Returns the content of the packet as String.
	 */
	public String toString() {
		return "Packet type: " + packetType + " (TableUpdate) - Length: " + packetLength + " - Value:\n" + "Next hop from " + srcName + " to " + dstName + ": " + nextHop;
	}

    public String getNextHop(){
        return nextHop;
    }

    public String getSavedMessage(){
        return savedMessage;
    }
}
