// Author: James Deasley
// Student ID: 19334721

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.net.InetSocketAddress;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;

import tcdIO.Terminal;

public class Router extends Node {
	static final int DEFAULT_PORT = 50001;

	// Terminal terminal;

    // Map of topics and addresses of Dashboards which are subsribed to them
    private Map<String, InetSocketAddress> forwardingTable;

	/*
	 *
	 */
	Router(int port, String firstKey, String firstHost, int firstPort) {
		try {
			// this.terminal = terminal;
			socket= new DatagramSocket(port);
            forwardingTable = new HashMap<>();
            forwardingTable.put(firstKey, new InetSocketAddress(firstHost, firstPort));
			listener.go();
		}
		catch(java.lang.Exception e) {e.printStackTrace();}
	}

	/**
	 * Assume that incoming packets contain a String and print the string.
	 */
	public void onReceipt(DatagramPacket packet) {
		try {
			System.out.println("\nReceived packet:");

			PacketContent content = PacketContent.fromDatagramPacket(packet);

            if(content.getType()==PacketContent.MESSAGE){
                String dstName = ((Message)content).getDstName();
                System.out.println("Forwarding packet to: " + dstName);

                DatagramPacket forward;
				forward = content.toDatagramPacket();

                /*
                 *
                 * I don't think this will work just yet as the forwarding table holds Strings as values, not Socket Addresses.
                 *
                 */
                

				forward.setSocketAddress(forwardingTable.get(dstName));
				socket.send(forward);
            }
		}
		catch(Exception e) {e.printStackTrace();}
	}


	public synchronized void start() throws Exception {
		System.out.println("Listening on port: " + DEFAULT_PORT);
        System.out.println("Forwarding table entry for 'trinity':\n" + 
            forwardingTable.get("trinity").toString());
        System.out.println("Waiting for contact...");
		this.wait();
	}

	/*
	 * arg[0]: First key for forwarding table
     * arg[1]: Host of first address for forwarding table
     * arg[2]: Port of first address for forwarding table
	 */
	public static void main(String[] args) {
		try {
			// Terminal terminal = new Terminal("Router");
			(new Router(DEFAULT_PORT, args[0], args[1], Integer.parseInt(args[2]))).start();
			System.out.println("Program completed");
		} catch(java.lang.Exception e) {e.printStackTrace();}
	}
}
